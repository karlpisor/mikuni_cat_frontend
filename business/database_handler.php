<?php
	// Class providing generic data access f(x)
	class DatabaseHandler
	{
		// Hold an instance of the PDO class
		private static $_mHandler;
		
		// Private constructor to prevent direct creation of object
		private function __construct()
		{
		}
		
		// Return an initialized database handler
		private static function GetHandler()
		{
			// Create a database connection only if one doesn't already exist
			if (!isset(self::$_mHandler))
			{
				// Execute code catching potential exceptions
				try
				{
					// Create a new PDO instance
					self::$_mHandler = new PDO (
										PDO_DSN, DB_USERNAME, DB_PASSWORD,
										array(PDO::MYSQL_ATTR_INIT_COMMAND =>"SET CHARACTER SET `utf8`"));
											  // not used "SET NAMES utf8"
					// Configure PDO to throw exceptions
					self::$_mHandler->setAttribute(PDO::ATTR_ERRMODE,
												   PDO::ERRMODE_EXCEPTION);	
					
				} catch (PDOException $e) {
					// Close the database handler and trigger an error
					self::Close();
					trigger_error($e->getMessage(), E_USER_ERROR);
				}
			}
			
			// Return the database handler object
			return self::$_mHandler;
		}
		
		// Clear the PDO class instance
		public static function Close()
		{
			self::$_mHandler = null;
		}
		
		// Wrapper method for PDOStatement::execute(); this is used for queries that don't return records
		public static function Execute($sqlQuery, 
									   $params = null)
		{
			// Try to execute an SQL query or a stored procedure
			try
			{
				// Get the database handler
				$database_handler = self::GetHandler();
				
				// Prepare the query for execution
				$statement_handler = $database_handler->prepare($sqlQuery);
				
				// Execute query
				// This is the built-in PDO method execute
				$statement_handler->execute($params);
				
			} // Trigger an error if an exception was thrown when executing the SQL query
			catch (PDOException $e) 
			{
				// Close the database handler and trigger an error
				self::Close();
				trigger_error($e->getMessage(), E_USER_ERROR);
			} 

		} // end Execute()
		
		// Wrapper method for PDOStatement::fetchAll()
		public static function GetAll($sqlQuery, 
									  $params = null,
									  $fetchStyle = PDO::FETCH_ASSOC)
		{
			// Initialize the return value to null
			$result = null;
			
			// Try to execute an SQL query or a stored procedure
			try
			{
				// Get the database handler
				$database_handler = self::GetHandler();
				
				// Prepare the query for execution
				$statement_handler = $database_handler->prepare($sqlQuery);
				
				// Execute query
				$statement_handler->execute($params);
				
				// Fetch result
				// This is the built-in PDO method fetchAll
				$result = $statement_handler->fetchAll($fetchStyle);
				
				// Count the number of columns in the result set
				$colcount = $statement_handler->columnCount();
				
				
			} // Trigger an error if an exception was thrown when executing the SQL query
			catch (PDOException $e) 
			{
				// Close the database handler and trigger an error
				self::Close();
				trigger_error($e->getMessage(), E_USER_ERROR);
			} 	
			
			// Return the query results
			return $result;
		} // end GetAll()
		
		// Wrapper method for PDOStatement::fetch()
		public static function GetRow($sqlQuery, 
									  $params = null,
									  $fetchStyle = PDO::FETCH_ASSOC)
		{
			// Initialize the return value to null
			$result = null;
			
			// Try to execute an SQL query or a stored procedure
			try
			{
				// Get the database handler
				$database_handler = self::GetHandler();
				
				// Prepare the query for execution
				$statement_handler = $database_handler->prepare($sqlQuery);
				
				// Execute query
				$statement_handler->execute($params);
				
				// Fetch result
				// This is the built-in PDO method fetch
				$result = $statement_handler->fetch($fetchStyle);
				
			} // Trigger an error if an exception was thrown when executing the SQL query
			catch (PDOException $e) 
			{
				// Close the database handler and trigger an error
				self::Close();
				trigger_error($e->getMessage(), E_USER_ERROR);
			} 	
			
			// Return the query results
			return $result;
		} // end GetRow()
		
		// Return the first column value from a row
		public static function GetOne($sqlQuery, 
									  $params = null)
		{
			// Initialize the return value to null
			$result = null;
			
			// Try to execute an SQL query or a stored procedure
			try
			{
				// Get the database handler
				$database_handler = self::GetHandler();
				
				// Prepare the query for execution
				$statement_handler = $database_handler->prepare($sqlQuery);
				
				// Execute query
				$statement_handler->execute($params);
				
				// Fetch result
				// This is the built-in PDO method fetch
				$result = $statement_handler->fetch(PDO::FETCH_NUM);
				
				// Save the first value of the result set
				$result = $result[0];
				
			} // Trigger an error if an exception was thrown when executing the SQL query
			catch (PDOException $e) 
			{
				// Close the database handler and trigger an error
				self::Close();
				trigger_error($e->getMessage(), E_USER_ERROR);
			} 	
			
			// Return the query results
			return $result;
		} // end GetOne()
		
	} // end class DatabaseHandler	
?>
					