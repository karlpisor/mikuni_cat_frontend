<div id="f">
      <div class="container">
        <div class="row centered footer-data-container">
          <!--<span class="footer-data phone"><?php //echo $Footer_PhoneText; ?></span>  -->
		  <span class="footer-data mail"><?php echo $Footer_MailText; ?></span><br>
		  <!--<span class="footer-data"><?php echo $Footer_OfficeText; ?></span>-->
          <h6 class="copyright mt">Copyright &copy;<?php echo date("Y"); ?>, KK Sherpa. All rights reserved.
          </h6>
        </div><!--/row-->
      </div><!--/container-->
    </div><!--/F-->

    <?php require_once 'js_page_closer.php';?>
  </body>
</html>
