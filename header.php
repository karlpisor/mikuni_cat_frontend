<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php echo $AllPages_Description; ?>" />
    <meta name="keywords" content="<?php echo $AllPages_Keywords; ?>" />
    <meta name="author" content="KK Sherpa, All rights reserved.">
    <link rel="icon" href="favicon.ico">

    <title><?php echo $AllPages_Header_Title; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="assets/css/ionicons.min.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/font-awesome.css" rel="stylesheet">
    
    <!-- Load language-based font settings -->
    <?php 
    // default user session is English
	if ((isset($_SESSION['UserLang'])) && ($_SESSION['UserLang'] == 'JP')){
		echo '<link href="assets/css/jp_fonts.css" rel="stylesheet">';
	} else {
		echo '<link href="assets/css/en_fonts.css" rel="stylesheet">';
	}?>


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

