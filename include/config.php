<?php
	
	// Enforce Tokyo timezone for date/time calculations
	date_default_timezone_set('Asia/Tokyo');
	
	// SITE_ROOT contains the full path to the root folder
	define('SITE_ROOT', dirname(dirname(__FILE__)));
	
	// Application directories
	define('PRESENTATION_DIR', SITE_ROOT .'/presentation/');
	define('BUSINESS_DIR', SITE_ROOT .'/business/');
	define('PANEL_DIR', SITE_ROOT .'/panels/');
	define('LANGUAGE_DIR', SITE_ROOT .'/language/');
	define('INCLUDE_DIR', SITE_ROOT .'/include/');
	
	
	
	// These should be true while developing the web site
	define('IS_WARNING_FATAL', true);
	define('DEBUGGING', true);
	
	// The error types to be reported
	define('ERROR_TYPES', E_ALL);
	//error_reporting(E_ALL);
	//ini_set('display_errors', 'On');
	
	// Settings about mailing the error messages to admin
	define('SEND_ERROR_MAIL', false);
	define('ADMIN_ERROR_MAIL', 'karl@missionpeoplesystems.com');
	define('SENDMAIL_FROM', 'system_admin@missionpeoplesystems.com');
	ini_set('sendmail_from', SENDMAIL_FROM);
	
	// By default we don't log errors to a file
	define('LOG_ERRORS', false);
	// TODO: set file path for errors file
	//define('LOG_ERRORS_FILE', '/home/username/tshirtshop/errors.log'); // Linux
	
	// Generic error message to be displayed instead of debug info (when DEBUGGING is false)
	define('SITE_GENERIC_ERROR_MESSAGE', '<h1>Mikuni Cat Skiing Error!</h1>');
	
	// Set host environment
	$host = 'local';
	
	// Define host environment variables
	if ($host == 'local') {
		
		// live AWS environment, 
		define('DB_SERVER', '54.248.93.97');
		define('DB_USERNAME', 'mpsguest');
		define('DB_PASSWORD', 'paintitblack');
		define('DB_DATABASE', 'sherpaDB');
		
		define('ENVIRONMENT', 'local');
		define('VIRTUAL_LOCATION', '/mikuni_cat_frontend/');
		define('PAGE_TITLE_STEM', 'Local - Mikuni Cat Front');
		
		define('REGISTRATION_IS_OPEN', true);
		define('MAIL_IS_LIVE', 0);
		define('SMTP_HOST','AWS_Tokyo');
		define('USE_SSL', false);
		
		// debugging
		$debugging = false;
		
	} else {
		echo "Error with environment config. Please notify site administrator.";
	}
	
	// Define database connection variables
	define('DB_PERSISTENCE', 1);
	define('PDO_DSN', 'mysql:host=' .DB_SERVER .';dbname=' .DB_DATABASE);
	
	// Server HTTP port (can omit if default 80 is used)
	define('HTTP_SERVER_PORT', '80');
	
	
?>