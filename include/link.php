<?php
	class Link
	{
		public static function Build($link, $type = 'http')
		{
			// The Build method strips down the URL and rebuilds it
			// The first parameter sets the URL string to be processed
			// The second parameter assigns http or https as the type of URL string to be processed
			// $base assigns http or https, then adds the base server name
			$base = (($type == 'http' || USE_SSL == 'false') ? 'http://' : 'https://') .
					getenv('SERVER_NAME');
		
			// VIRTUAL_LOCATION is the folder in which the site resides
			// this is /eng_adv_v2/ for the local dev site
			$link = $base .VIRTUAL_LOCATION .$link;
		
			return $link;
		}
		
		// Enforce page to be accessed through HTTPS
		public static function EnforceSSL()
		{
			// Enforce page to be accessed through HTTPS if USE_SSL is on
			if (USE_SSL == 'true' && getenv('HTTPS') != 'on'){
				
				// SSL is live, and the server is NOT in HTTPS mode
				// This switches the browser mode to secure
				header('Location: https://'
					.getenv('SERVER_NAME')
					.getenv('REQUEST_URI'));
				exit();	
			}
		}
		
		// Enforce page to be accessed through HTTP
		public static function RelaxSSL()
		{
			// Enforce page to be accessed through HTTP if USE_SSL is on
			if (USE_SSL == 'true' && getenv('HTTPS') == 'on'){
				
				// SSL is live, and the server is in HTTPS mode
				// This forces the page to come back to normal mode
				header('Location: http://'
					.getenv('SERVER_NAME')
					.getenv('REQUEST_URI'));
				exit();	
			}
		}
		
		
		// Base links to programs, pages
		public static function ToProgram($programId)
		{
			$link = self::CleanURLText(Content::GetProgramTitle($programId)) .'_program' .$programId .'/';
			
			return self::Build($link);
		}
		
		public static function ToPage($pageId)
		{
			$link = self::CleanURLText(Content::GetPageTitle($pageId)) .'_page' .$pageId .'/';
				
			return self::Build($link);
		}
		
		
		// Prepares a string to be included in a URL
		public static function CleanURLText($string)
		{
			// Remove all unacceptable characters
			$not_acceptable_characters = '';
			// preg_replace these guys here
			
			// Remove all leading and trailing spaces
			$string = trim($string);
			
			// Change all dashes, underscores, and spaces to dashes
			$string = preg_replace('#[-_ :()]#', '-', $string);
			
			// Remove all parentheses
			$string = preg_replace('#[()]#', '', $string);
			
			// Return the modified string
			return strtolower($string);
		}
		
		
		// Redirects to proper URL if not already there
		public static function CheckRequest()
		{
			$proper_url = '';
			
			// Obtain proper URL for program pages
			if (isset($_GET['ProgramId']))
			{
				$proper_url = self::ToProgram($_GET['ProgramId']);
			}
			elseif (isset($_GET['PageId']))
			{
				$proper_url = self::ToPage($_GET['PageId']);
			}
			
			// Remove the virtual location from the URL so we can compare paths
			//$requested_url = self::Build(str_replace(VIRTUAL_LOCATION, '', $_SERVER['REQUEST_URI']));
				
			//echo $requested_url;
			
			// Clean output buffer
			ob_clean();
			
			// Redirect 301
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: ' .$proper_url);
			
			// Clear the buffer and stop execution
			flush();
			ob_flush();
			ob_end_clean();
			exit();
		
			
			
			// 301 redirect to the proper URL if necessary
			/* if ($requested_url != $proper_url)
			{
				// Clean output buffer
				ob_clean();
				
				// Redirect 301
				header('HTTP/1.1 301 Moved Permanently');
				header('Location: ' .$proper_url);
				
				// Clear the buffer and stop execution
				flush();
				ob_flush();
				ob_end_clean();
				exit();
			} */
		
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}
?>