<?php 

	// Activate session
	session_start();
	
	// Include utility files
	require_once 'include/config.php';
	
	// Use normal, non-SSL connection for this page
	require_once 'include/link.php';
	Link::RelaxSSL();
	
	// Set language strings
	require_once LANGUAGE_DIR .'setLanguage.php';
	require_once LANGUAGE_DIR .'lang.public.php';
	
	// Set the error handler
	require_once BUSINESS_DIR .'error_handler.php';
	ErrorHandler::SetHandler();
	
	// Load the database handler
	require_once BUSINESS_DIR .'database_handler.php';
	
	// Load business tier
	require_once BUSINESS_DIR .'content.php';


	
	
	
	
	
	
	// ------------------ HTML begins here ---------------------------------------------- //
	require_once 'header.php';?>

  <body>
	<nav class="navbar navbar-default navbar-fixed-top">
	
		<div class="container">
            
            	<div class="navbar-header">
                
	                <!-- logo -->
		            <a href="index.php"><div id="logo"></div></a>
		            
					
				
				</div> 
                
                <div class="nav navbar-nav navbar-right">
					
						<?php require PANEL_DIR .'banner_lang_toggle.php';?>
                </div> 
                    
			        
         </div>
         
	</nav>



    <div id="h">
      <h1 id="frontpage-title" class="font-lato"><?php echo $BigBanner_Title; ?></h1>
      <h4 id="frontpage-tagline" class="font-lato"><?php echo $BigBanner_Content; ?></h4>
          
    </div><!-- /H -->
    
    
    
    <?php require PANEL_DIR .'page_navigation.php';?>
    
    
    <div class="container mt mb">
        <div class="row">
        
          <div class="col-md-6 col-md-offset-1">
          <h3 class="leader-subtitle"><?php echo $MikuniStory_Subtitle; ?></h3>
          <h2 class="font-lato"><?php echo $MikuniStory_Title; ?></h2>
          <?php echo $MikuniStory_Body; ?>
          </div>
          
        </div><!--/row-->
        
        <div class="row">
          <div class="col-md-6 col-md-offset-1">
                <!--<a class="btn btn-primary button-advance" href="#"><?php //echo $Button_ViewAvailability; ?>
                </a>-->
          </div>
        </div><!--/row-->
        
        
    </div><!--/container-->
    
    
    <div class="container mt mb">
        <div class="row">
        
          <div class="col-md-6 col-md-offset-1">
          <h3 class="leader-subtitle"><?php echo $OurGuides_Subtitle; ?></h3>
          <h2 class="font-lato"><?php echo $OurGuides_Title; ?></h2>
          <?php echo $OurGuides_Body; ?>
          </div>
          
        </div><!--/row-->
        
        <!-- anchor -->
        <a name="pricing-details-content"></a>
        
    </div><!--/container-->
    
    <div class="container mt mb">
        <div class="row">
          <div class="col-md-6 col-md-offset-1">
          <div class="front-image"><img src="assets/img/group-in-front-of-cat.jpg"></div>
          <h3 class="leader-subtitle"><?php echo $TourDetails_Subtitle; ?></h3>
          <h2 class="font-lato"><?php echo $TourDetails_Title; ?></h2>
          <p class=""><?php echo $TourDetails_Body; ?></p>
        <table class="timetable <?php if($_SESSION['UserLang'] == 'JP'){ echo 'jp-details'; }?>">
		<tbody>
			
			<tr>
				<td><?php echo $TourDetails_Price_Label; ?></td>
				<td><?php echo $TourDetails_Price_Desc; ?></td>
			</tr>
			
			<tr>
				<td><?php echo $TourDetails_Includes_Label; ?></td>
				<td><?php echo $TourDetails_Includes_Desc; ?></td>
			</tr>
			
			<tr>
				<td><?php echo $TourDetails_MinMaxParticipants_Label; ?></td>
				<td><?php echo $TourDetails_MinMaxParticipants_Desc; ?></td>
			</tr>
			
			<tr>
				<td><?php echo $TourDetails_MinAge_Label; ?></td>
				<td><?php echo $TourDetails_MinAge_Desc; ?></td>
			</tr>
			
			<tr>
				<td><?php echo $TourDetails_AbilityLevel_Label; ?></td>
				<td><?php echo $TourDetails_AbilityLevel_Desc; ?></td>
			</tr>
			
			<tr>
				<td><?php echo $TourDetails_StartTime_Label; ?></td>
				<td><?php echo $TourDetails_StartTime_Desc; ?></td>
			</tr>
			
			<tr>
				<td><?php echo $TourDetails_VerticalDrop_Label; ?></td>
				<td><?php echo $TourDetails_VerticalDrop_Desc; ?></td>
			</tr>
			
			<tr>
				<td><?php echo $TourDetails_NumRuns_Label; ?></td>
				<td><?php echo $TourDetails_NumRuns_Desc; ?></td>
			</tr>
			
			<tr>
				<td><?php echo $TourDetails_Cancellation_Label; ?></td>
				<td><?php echo $TourDetails_Cancellation_Desc; ?></td>
			</tr>
			
			<tr>
				<td><?php echo $TourDetails_Lunch_Label; ?></td>
				<td><?php echo $TourDetails_Lunch_Desc; ?></td>
			</tr>
			
			<tr>
				<td class="border-bottom"><?php echo $TourDetails_Options_Label; ?></td>
				<td class="border-bottom"><?php echo $TourDetails_Options_Desc; ?></td>
			</tr>
			
			
		</tbody>			
		</table>
	
          
          
        </div>
        </div><!--/row-->
        
        
        
        
        <div class="row">
          <div class="col-md-6 col-md-offset-1">
                <!--<a class="btn btn-primary button-advance" href="#"><?php //echo $Button_ViewAvailability; ?>
                </a>-->
          </div>
          
          <!-- anchor -->
          <a name="faq-content"></a>
          
        </div><!--/row-->
        
          
    </div><!--/container-->
    
    <div class="container mt mb">
        <div class="row">
          <div class="col-md-6 col-md-offset-1">
          
          <h3 class="leader-subtitle"><?php echo $TourSchedule_Subtitle; ?></h3>
          <h2 class="faq-question font-lato"><?php echo $TourSchedule_Title; ?></h2>
		  <p class="faq-answer"><?php echo $TourSchedule_Body; ?></p>
			<table class="timetable">
				<tbody>
					<tr>
						<td>9:00am</td>
						<td><?php echo $TourSchedule_Step1; ?></td>
					</tr>
					<tr>
						<td>9:30am</td>
						<td><?php echo $TourSchedule_Step2; ?></td>
					</tr>
					<tr>
						<td>10:00am</td>
						<td><?php echo $TourSchedule_Step3; ?></td>
					</tr>
					<tr>
						<td>10:30am</td>
						<td><?php echo $TourSchedule_Step4; ?></td>
					</tr>
					<tr>
						<td>11am - 3pm</td>
						<td><?php echo $TourSchedule_Step5; ?></td>
					</tr>
					<tr>
						<td>3:00pm</td>
						<td><?php echo $TourSchedule_Step6; ?></td>
					</tr>
					
					<tr>
						<td class="border-bottom">4:00pm</td>
						<td class="border-bottom"><?php echo $TourSchedule_Step7; ?></td>
					</tr>
				</tbody>			
			</table>
			
			
			<h2 class="faq-question font-lato mt"><?php echo $SafetyEquipment_Title; ?></h2>
			<p class="faq-answer"><?php echo $SafetyEquipment_Body; ?></p>
			<table class="timetable <?php if($_SESSION['UserLang'] == 'JP'){ echo 'jp-details'; }?>">
				<tbody>
					<tr>
						<td><?php echo $SafetyEquipment_Beacon_Label; ?></td>
						<td><?php echo $SafetyEquipment_Beacon_Desc; ?></td>
					</tr>
					<tr>
						<td><?php echo $SafetyEquipment_Probe_Label; ?></td>
						<td><?php echo $SafetyEquipment_Probe_Desc; ?></td>
					</tr>
					<tr>
						<td><?php echo $SafetyEquipment_Shovel_Label; ?></td>
						<td><?php echo $SafetyEquipment_Shovel_Desc; ?></td>
					</tr>
					<tr>
						<td><?php echo $SafetyEquipment_Backpack_Label; ?></td>
						<td><?php echo $SafetyEquipment_Backpack_Desc; ?></td>
					</tr>
					<tr>
						<td class="border-bottom"><?php echo $SafetyEquipment_OtherGear_Label; ?></td>
						<td class="border-bottom"><?php echo $SafetyEquipment_OtherGear_Desc; ?></td>
					</tr>
					
				</tbody>			
			</table>
			
			
			<h2 class="faq-question font-lato mt"><?php echo $RecommendedGear_Title; ?></h2>
			<p class="faq-answer"><?php echo $RecommendedGear_Body; ?></p>
			<table class="timetable <?php if($_SESSION['UserLang'] == 'JP'){ echo 'jp-details'; }?>">
				<tbody>
					<tr>
						<td><?php echo $RecommendedGear_Helmet_Label; ?></td>
						<td><?php echo $RecommendedGear_Helmet_Desc; ?></td>
					</tr>
					
					<tr>
						<td><?php echo $RecommendedGear_Skis_Label; ?></td>
						<td><?php echo $RecommendedGear_Skis_Desc; ?></td>
					</tr>
					<tr>
						<td><?php echo $RecommendedGear_SkiPoles_Label; ?></td>
						<td><?php echo $RecommendedGear_SkiPoles_Desc; ?></td>
					</tr>
					<tr>
						<td><?php echo $RecommendedGear_Snowboard_Label; ?></td>
						<td><?php echo $RecommendedGear_Snowboard_Desc; ?></td>
					</tr>
					<tr>
						<td><?php echo $RecommendedGear_SkiWear_Label; ?></td>
						<td><?php echo $RecommendedGear_SkiWear_Desc; ?></td>
					</tr>
					<tr>
						<td><?php echo $RecommendedGear_Facemask_Label; ?></td>
						<td><?php echo $RecommendedGear_Facemask_Desc; ?></td>
					</tr>
					<tr>
						<td><?php echo $RecommendedGear_Goggles_Label; ?></td>
						<td><?php echo $RecommendedGear_Goggles_Desc; ?></td>
					</tr>
					<tr>
						<td class="border-bottom"><?php echo $RecommendedGear_RentingGear_Label; ?></td>
						<td class="border-bottom"><?php echo $RecommendedGear_RentingGear_Desc; ?></td>
					</tr>
				</tbody>			
			</table>	
			
			
			<h2 class="faq-question font-lato mt"><?php echo $HowToGetHere_Title; ?></h2>
			<p class="faq-answer"><?php echo $HowToGetHere_Body; ?></p>
			
			
			
			<h2 class="faq-question font-lato mt"><?php echo $AddlQuestions_Title; ?></h2>
			<p class="faq-answer"><?php echo $AddlQuestions_Body; ?></p>
			
			
			
        </div><!--/column-->
        </div><!--/row-->
        
        <div class="row">
          <div class="col-md-6 col-md-offset-1">
                <!--<a class="btn btn-primary button-advance" href="#"><?php //echo $Button_ViewAvailability; ?>
                </a>-->
          </div>
        </div><!--/row-->
        
    </div><!--/container-->

    

    <div id="testimonials">
      <div class="container mt">
        <div class="row">
          <div class="col-md-6 col-md-offset-3 centered">
            
            
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
              <!-- Wrapper for slides -->
              <div class="carousel-inner">
                <div class="item active">
                  <h3>&ldquo;This is a whole different level of skiing.&rdquo;</h3>
                  <h5><tgr>NANCY D. (skis, first backcountry experience)</tgr></h5>
                </div>
                <div class="item">
                  <h3>&ldquo;Mikuni is amazing! This is deep powder paradise.&rdquo;</h3>
                  <h5><tgr>EDDIE M. (powder skis, loves off-piste powder)</tgr></h5>
                </div>
                <div class="item">
                  <h3>&ldquo;With my friends, there's no better way to spend a day in the snow.&rdquo;</h3>
                  <h5><tgr>DANA C. (snowboard, backcountry convert)</tgr></h5>
                </div>
              </div>
            </div><!--/Carousel-->

          </div>
        </div><!--/row-->
      </div><!--/container-->
    </div><!--/green-->

    <?php require_once 'footer.php';?>