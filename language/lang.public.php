<?php
	// default language strings are JP, below in else statement
	if ((isset($_SESSION['UserLang'])) && ($_SESSION['UserLang'] == 'EN')){
		
		// SEO: keywords and description
		$AllPages_Keywords = 'Japan, cat skiing, heliski, heliskiing, Mikuni';
		$AllPages_Description = 'Mikuni Cat runs backcountry skiing and snowboarding tours in the historic Mikuni ski area just two and a half hours from Tokyo.';
		$AllPages_Header_Title = 'Mikuni Cat, Backcountry for Everyone';
		
		// page navigation
		$BigBanner_Title = 'Powder dreams.<br>Made fresh daily.';
		$BigBanner_Content = 'Exclusive access to great terrain and deep powder. <br>Only 2 hours from Tokyo.';
		$Nav_Questions = 'A Day with Us';
		$Nav_Availability = 'Book Now';
		$BookNow_Email_HREF = 'mailto:reservations@mikuni-cat.com?subject=Mikuni Cat Skiing Reservations&body=
I\'m ready for the backcountry! Please reserve my place(s) for Mikuni Cat Skiing.%0A%0A
Name:%0A
Requested Tour Date(s):%0A
Number of Guests:%0A
Phone number:%0A
%0A
Thank you for your email. A Mikuni Cat representative will contact you as soon as possible.%0A%0A
The Mikuni Cat Team';
		
		$Nav_Pricing_Details = 'Pricing<br>and Tour Details';
		$Button_ViewAvailability = 'View Current Availability';
		
		
		// the mikuni story
		$MikuniStory_Subtitle = 'The Mikuni Experience';
		$MikuniStory_Title = 'What would a day on the mountain feel like if the ski lift had never been invented?';
		$MikuniStory_Body = '<p>At Mikuni Cat, we often wondered what our world would look like if the ski lift had never been invented. What if we just focused on good friends, great snow, and enjoying ourselves in the backcountry? What if we left room to enjoy the quiet rush of gliding through deep powder, or took the time to greet a friendly <em>kamoshika</em>? Twenty years ago, Mikuni was a ski area, but as the lifts and structures were pulled out it has been slowly reclaimed by nature. Today, it is not a ski area and it is not a remote wilderness; rather, it is something in between, something new, as if the ski lift had never been invented.</p>
<p>There are no lines, no J-pop and no stress. Just perfect virgin snow, miles from paved roads, with friendly people who love being in the winter mountains. Mikuni is where we bring our friends and introduce them to our backcountry experience at their pace. With only 8 guests per day, our emphasis is on quality over quantity: gentle, intermediate terrain and trees, the right number of runs for the conditions, and a whole lot of fun. </p>
<p>Conversely, if you are looking to huck cliffs, or expose yourself to high avalanche danger and complex terrain, Mikuni is not the place for you. While we do have some expert terrain, we pride ourselves on being accessible to a broad range of skill levels, from first-timers through upper-intermediate backcountry enthusiasts. </p>
<p>Our professional team can also charter days for corporate groups, independently guided groups, and film crews. If you would like to bring your group to Mikuni, please <a href="mailto:reservations@mikuni-cat.com">get in touch</a> and tell us what you would like to do, we will do our best to find a way to make it work.</p>';

		
		// guide introduction
		$OurGuides_Subtitle = 'Introducing Your Guides';
		$OurGuides_Title = 'Tom and Kiyoko, from Chamonix, France';
		$OurGuides_Body = '<p>Tom, our lead guide, is an UIMLA International Mountain Leader, a CASI and ASSI ski instructor, and holds AAIRE Level 2 Avalanche and Wilderness First Responder first aid certifications. He is a veteran of many big ski descents in the French Alps, including the north face of Mont Blanc and the Voie Eugster on the north face of the Aiguille du Midi.</p>
<p>Kiyoko, our tail guide, is AAIRE trained and is also a qualified first aider. Tom and Kiyoko split their time between backcountry skiing in and around Mikuni in Winter and guiding on the Tour Du Mont Blanc in Chamonix in summer, and can often be found on many of the alpine peaks in between.
</p><p>
Several other guides also work at Mikuni from time to time during the season depending on schedules as we build out the team and study expansion of our terrain tenure. 
</p>';
		
		
		
		
		
		// pricing and tour details
		$TourDetails_Subtitle = 'Pricing and Tour Details';
		$TourDetails_Title = 'Due to strong demand, we recommend that you book well in advance.';
		$TourDetails_Body = 'Mikuni Cat conducts one tour per day, up to 8 guests per tour. All tours are led by a lead guide and a tail guide. Tour pricing and details:';
		
		$TourDetails_Price_Label = 'Price';
		$TourDetails_Price_Desc = '¥25,000 per guest';
		$TourDetails_Includes_Label = 'Includes';
		$TourDetails_Includes_Desc = 'Cat rides, guides, drinks and snacks in the cat on the way back up after each run. We don’t serve a sit-down lunch so you can get the most time on snow. Feel free to bring your favorite trail snacks and a bottle of water.';
		
		
		$TourDetails_MinMaxParticipants_Label = 'Participants';
		$TourDetails_MinMaxParticipants_Desc = 'Minimum 3 guests, up to 8 guests maximum';
		$TourDetails_MinAge_Label = 'Skier age';
		$TourDetails_MinAge_Desc = 'Minimum 16 years old, guests under 20 years old require parental consent.';
		
		$TourDetails_AbilityLevel_Label = 'Skier ability level';
		$TourDetails_AbilityLevel_Desc = 'Intermediate or better skier. Skiing area is a former ski resort, slopes are intermediate level.';
		
		$TourDetails_StartTime_Label = 'Start time';
		$TourDetails_StartTime_Desc = '9:00am. We will pick you up at your hotel in the Naeba area. Please advise if you are staying at the Naeba Prince Hotel. If you plan to drive up on the day, please let us know and we will help you with parking arrangements.';
		$TourDetails_VerticalDrop_Label = 'Vertical drop';
		$TourDetails_VerticalDrop_Desc = 'The old ski area base starts at 1120m, the highest in the area, and we have about 450m of vertical drop.';
		$TourDetails_NumRuns_Label = 'Number of runs';
		$TourDetails_NumRuns_Desc = 'Typically 5-7 runs per day, depending on skier ability, weather, and terrain conditions.';
		$TourDetails_Cancellation_Label = 'Cancellation policy';
		$TourDetails_Cancellation_Desc = 'The following cancellation fees apply to all Mikuni Cat tours:<br>
Cancellation fees are calculated according to number of days prior to your registered tour:<br>
Cancellation 1 day prior to start date, or on date of tour: 100% charge.<br>
Cancellation 2 days prior to start date: 50% charge.<br>
Cancellation 3-4 days prior to start date: 30% charge.<br><br>
<strong>Forced Cancellation: </strong>If Mikuni Cat cancels a tour on the day due to safety or weather concerns, we will issue a you a rain check good for a future tour within 1 year.';
		
		$TourDetails_Lunch_Label = 'Lunch';
		$TourDetails_Lunch_Desc = 'We prefer that guests eat on the go so that energy levels do not drop during the tour. We find that the cat ride up gives us the time to grab a snack and a drink and get recharged before the next run. Rather than take the traditional 1 hour break in the middle of the day, this approach gives us more time on snow.';
		$TourDetails_Options_Label = 'Options';
		$TourDetails_Options_Desc = 'Avalanche safety set and backpack, &yen;5,000/day';
		
		
		
		
		
		// tour daily schedule
		$TourSchedule_Subtitle = 'Planning Your Day';
		$TourSchedule_Title = 'How is the tour schedule set up?';
		$TourSchedule_Body = 'Here\'s a sample tour day schedule. Please note that while 9am start time is fixed, the rest of the schedule may change without notice due to weather conditions and/or other events, at the lead guide\'s discretion.';
		
		$TourSchedule_Step1 = 'Everybody has arrived in the Backcountry Center in Naeba Town. If you need a pickup in town or a Naeba Prince Hotel pickup, please let us know.';
		$TourSchedule_Step2 = 'Complete check-in and daily safety briefing';
		$TourSchedule_Step3 = 'Depart to cat rendezvous point';
		$TourSchedule_Step4 = 'Beacon search test';
		$TourSchedule_Step5 = 'Runs and fun';
		$TourSchedule_Step6 = 'Last uphill cat';
		$TourSchedule_Step7 = 'Everybody off the mountain and into a local establishment for a celebratory beverage.';
		$TourSchedule_Step8 = '';
		
		
		
		
		
		
		// safety equipment
		$SafetyEquipment_Title = 'What safety equipment do I need?';
		$SafetyEquipment_Body = 'We ask that all participants bring a backpack with a complete set of avalanche safety gear as shown below. A rental set (¥5,000/day) is available; please select this option when you place your order. Note that all guest-supplied safety equipment will be inspected before the tour begins. If your equipment is deemed not suitable, then you may be required to rent the school\'s rental safety set. Here are the required components:';
		
		$SafetyEquipment_Beacon_Label = 'Beacon';
		$SafetyEquipment_Beacon_Desc = 'Modern transceiver beacon, with sufficient battery life.<br>
We recommend the <a class="link-blue" target="_blank" href="https://www.mammut.ch/INT/en/B2C-Kategorie/Women/ELEMENT-Barryvox/p/2710-00040-1012-1">Mammut Element Barryvox</a> or the <a class="link-blue" href="https://www.amazon.com/Backcountry-Access-Tracker-Avalanche-Beacon/dp/B000YUATNO">DTS Avalanche Beacon</a>.';
		$SafetyEquipment_Probe_Label = 'Snow probe';
		$SafetyEquipment_Probe_Desc = '220cm - 300cm length<br>
We recommend the <a class="link-blue" target="_blank" href="https://www.ortovox.com/uk/shop/avalanche-emergency-equipment/avalanche-probes/">Ortovox 240</a>.';
		$SafetyEquipment_Shovel_Label = 'Shovel';
		$SafetyEquipment_Shovel_Desc = 'Collapsible hand shovel, aluminum or other light metal construction<br>
We recommend the <a class="link-blue" target="_blank" href="http://www.pieps.com/en/product/pieps-shovel-tour-i-tour-t">Pieps Tour</a>.';
		
		$SafetyEquipment_Backpack_Label = 'Backpack';
		$SafetyEquipment_Backpack_Desc = 'Something to hold your avalanche equipment, camera, and water bottle.';
		$SafetyEquipment_OtherGear_Label = 'Other Gear';
		$SafetyEquipment_OtherGear_Desc = 'If you have airbags or other avalanche safety gear please feel free to bring it along and let you guide know upon check in what you have brought. ';
		
		
		// recommended gear
		$RecommendedGear_Title = 'What ski gear do you recommend?';
		$RecommendedGear_Body = 'As weather changes suddenly on the mountain, we recommend that all guests bring additional layers of clothing. For specific gear, these are our up-to-date recommendations:';
		
		$RecommendedGear_Helmet_Label = 'Helmet';
		$RecommendedGear_Helmet_Desc = 'Ski safety helmet, well fitting and warm. Most tours will ski trees so helmets are HIGHLY recommended!';
		$RecommendedGear_Skis_Label = 'Skis';
		$RecommendedGear_Skis_Desc = 'Off-piste or all-mountain skis, 90mm underfoot or wider';
		
		$RecommendedGear_SkiPoles_Label = 'Ski poles';
		$RecommendedGear_SkiPoles_Desc = 'Ski poles with powder (large) baskets if possible';
		$RecommendedGear_Snowboard_Label = 'Snowboard';
		$RecommendedGear_Snowboard_Desc = 'All-mountain or powder (preferably not alpine or freestyle)';
		$RecommendedGear_SkiWear_Label = 'Ski wear';
		$RecommendedGear_SkiWear_Desc = 'Waterproof shell and many layers so you can adjust to changing temperatures from weather and effort';
		$RecommendedGear_Facemask_Label = 'Facemask';
		$RecommendedGear_Facemask_Desc = 'Recommended. A lot of our best powder is when it\'s actually snowing and windy, so a facemask can really help to keep your face warm';
		
		$RecommendedGear_Goggles_Label = 'Goggles';
		$RecommendedGear_Goggles_Desc = 'To keep the powder out of your eyes and help you enjoy the view!';
		
		$RecommendedGear_RentingGear_Label = 'Renting Gear';
		$RecommendedGear_RentingGear_Desc = 'There are several rental shops in the Naeba Prince Hotel and around Naeba Town so if you need to rent anything, please arrange this the day before the tour. On the morning of your tour, there will not be time before the tour departs to arrange rental gear.';
		
		
		// how to get here
		$HowToGetHere_Title = 'How to Get to Naeba';
		$HowToGetHere_Body = 'We operate out of Naeba Town, so getting here is a breeze. Just take the Joetsu Shinkansen to Echigo Yuzawa Station (70-100 minutes from Tokyo) then the bus to Naeba (about 30 minutes). Or you can drive to the Tsukiyono Exit on the Kanetsu Expressway and follow the signs to Naeba. Putting the Naeba Prince Hotel into your car’s navigation (<a target="_blank" href="https://goo.gl/maps/evSvUuS71Bp">Google Maps link</a>) will get you right into town. Our Backcountry Center office is near the town’s main Asagai stoplight.';
		
		
		// additional questions
		$AddlQuestions_Title = 'I\'ve got a question you haven\'t answered!';
		$AddlQuestions_Body = 'We\'re here to help. Please don\'t hesitate to write us at <a href="mailto:reservations@mikuni-cat.com">reservations@mikuni-cat.com</a>.</p>';
		
		
		// testimonials
		
		
		
		// footer
		$Footer_PhoneText = 'Phone: +81 (0)70-5550-8807';
		$Footer_MailText = 'Mail: <a class="link-blue" href="mailto:reservations@mikuni-cat.com">reservations@mikuni-cat.com</a>';
		$Footer_OfficeText = 'Office hours: Weekdays, 8:30 to 16:00<br>Weekends/holidays, 8:00 to 17:00';
		
		
	} else {
		
		// SEO: keywords and description
		$AllPages_Keywords = 'JP_Japan, cat skiing, heliski, heliskiing, Mikuni';
		$AllPages_Description = 'JP_Mikuni Cat runs backcountry skiing and snowboarding in the historic Mikuni ski area just two and a half hours from Tokyo.';
		$AllPages_Header_Title = 'JP_Mikuni Cat, Backcountry for Everyone';
		
		// page navigation
		$BigBanner_Title = 'この銀世界、独り占めしない？';
		$BigBanner_Content = '一日８名が味わえる新雪の食べ放題。<br>東京駅から約２時間。';
		
		$Nav_Questions = 'よくあるご質問';
		$Nav_Availability = '今すぐお申込';
		$BookNow_Email_HREF = 'mailto:reservations@mikuni-cat.com?subject=三国キャットスキー：予約お申し込み&body=三国キャットスキー 予約係%0A%0Aお申し込みの手続きをお願いします。%0A%0A
お名前%0A
（漢字）：%0A
（ローマ字）：%0A
ご希望の日にち ：%0A
人数：%0A
お電話番号：%0A%0A
ご連絡ありがとうございます。ご予約の内容を確認するため、弊社からご連絡いたしますので、少々お待ちください。%0A%0A
三国キャットスキーチーム 一同';
		$Nav_Pricing_Details = '料金とツアーの詳細';
		$Button_ViewAvailability = 'スケジュールと空き席を表示する';
		
		
		// the mikuni story
		$MikuniStory_Subtitle = '三国のバックカントリー';
		$MikuniStory_Title = '静かな山の最高のロケーションで、自由に滑り尽くす、贅沢な雪山遊び。';
		$MikuniStory_Body = '<p>東京から車や新幹線でわずか２時間程度、苗場スキー場の奥数キロにある、旧三国スキー場跡地を使います。 スノーキャットで山を登り、標高1120mで始まる旧三国スキー場の跡地に到着します。オープンバーンからツリーランまでいくつかのコースが楽しめます。</p>
				
<p class="bold-callout"><strong>こんな方にお勧めします。</strong></p>
<ul class="bold-callout-list">
<li><i class="fa fa-check"></i>非圧雪のパウダースノーをゆったりと楽しみたい方。</li>
<li><i class="fa fa-check"></i>オープンバーンの滑走が大好きな方</li>
<li><i class="fa fa-check"></i>ツリーランも入ってみたい方</li>
<li><i class="fa fa-check"></i>バックカントリーをしてみたいけど自信がない方。<br>（滑走レベル中級以上）</li>
</ul>';
		
		// guide introduction
		$OurGuides_Subtitle = 'ガイドのご紹介';
		$OurGuides_Title = '安全第一の滑りをサポートするガイドのご紹介';
		$OurGuides_Body = '<p><strong>トム・グリーンオール（Tom Greenall）</strong><br>
英国出身、2000年よりフランス・シャモニーに在住。Aiguille du Midi北壁/モンブラン北壁/オートルート、シャモニー〜ツェルマットなどのバックカントリスキー経験あり。スキーは5歳からはじめ現在38年目。ヨーロッパを中心に北米、オセアニアなどの雪山を経験し、今回が2度目の日本滞在。シャモニーではIdris Skis(イドリススキー)というブランドで、環境に優しい全て手作りのスキーを制作しながら、UIMLA International Mountain Leaderとしてシャモニー周辺の山を中心にガイドして活躍中。
</p>

<p class="bold-callout">保有資格</p>
<ul class="bold-callout-list">
<li><i class="fa fa-circle"></i>UIMLA International Mountain Leader </li>
<li><i class="fa fa-circle"></i>アメリカ雪崩教育研究協会（AAIRE）認定レベル2</li>
<li><i class="fa fa-circle"></i>ウィルダネス・ファーストエイド</li>
<li><i class="fa fa-circle"></i>CASI カナダスキーインストラクター</li>
<li><i class="fa fa-circle"></i>ASSI スキーインストラクター</li>


</ul>


<p>
<strong>山口　きよ子</strong><br>
2010年からシャモニーに在住をきっかけに、バックカントリースキーを始める。
</p>

<p class="bold-callout">保有資格</p>
<ul class="bold-callout-list">
<li><i class="fa fa-circle"></i>アメリカ雪崩教育研究協会（AAIRE）認定レベル１</li>
<li><i class="fa fa-circle"></i>Snow Farmノルディックスキー・インストラクター（ニュージーランド）</li>
<li><i class="fa fa-circle"></i>MEDIC First Aideメディック・ファーストエイド</li>
</ul>






<p>
<strong>岩崎 龍大（イワサキ・リュウタ）</strong><br>
十数年フリースタイルプロスキーヤーとして活動し、パーク、ストリート、バックカントリー共に活動していたが、徐々にバックカントリーフィールドいることが多くなり、最近ではバックカントリー中心のスキー活動となる。三国キャットエリアの開拓をした。
</p>

<p class="bold-callout">保有資格</p>
<ul class="bold-callout-list">
<li><i class="fa fa-circle"></i>ウイルダネスファーストエイド</li>
</ul>
';
		
		
		
		
		
		// pricing and tour details
		$TourDetails_Subtitle = '料金とツアーの詳細';
		$TourDetails_Title = '人気のツアーがすぐ満席になりますので、お早めに予約をおすすめします。';
		$TourDetails_Body = '1日１組８名までのツアーとなります。 （１ツアーにバックカントリーガイド２名付きます）';
		
		$TourDetails_Price_Label = '料金';
		$TourDetails_Price_Desc = '¥25,000/1名';
		$TourDetails_Includes_Label = '料金に含まれるもの';
		$TourDetails_Includes_Desc = 'ガイディング、キャットオペレーション、保険';
		
		$TourDetails_MinMaxParticipants_Label = '参加人数';
		$TourDetails_MinMaxParticipants_Desc = '最小3名　最大8名';
		
		$TourDetails_MinAge_Label = '参加年齢';
		$TourDetails_MinAge_Desc = '１６歳以上、２０歳未満は保護者の同意が必要です';
		$TourDetails_AbilityLevel_Label = 'スキーレベル';
		$TourDetails_AbilityLevel_Desc = 'ゲレンデで中級以上で非圧雪バーンをコントロールして滑れる';
		
		$TourDetails_StartTime_Label = '開始時刻';
		$TourDetails_StartTime_Desc = '9:00am';
		$TourDetails_VerticalDrop_Label = '滑走高度';
		$TourDetails_VerticalDrop_Desc = '450m+';
		$TourDetails_NumRuns_Label = '滑走回数';
		$TourDetails_NumRuns_Desc = '1日5〜7回（天気やコース状態、参加者の能力などによって変わります）';
		$TourDetails_Cancellation_Label = 'キャンセルについて';
		$TourDetails_Cancellation_Desc = 'ツアーのキャンセル料は以下の通りです、ご理解いただいた上でのご予約をお願いします。<br>
当日または開催２日前までの１７時以降：100%<br>
２日前の１７時まで：50%<br>
４日前までの１７時まで：30%<br>
その前：0%<br>';
		
		
		$TourDetails_Lunch_Label = 'ランチ';
		$TourDetails_Lunch_Desc = 'ランチをガッツリ取る方針ではなく、少しずつキャットの中で軽食、おやつ、ドリンクをとって、体力をキープする方法をとっています。昼もの一時間のランチ休憩を取れば、体が冷え、体力が落ち、怪我の原因となるためです。休憩が必要な方は滑らすにいつでもキャットで休憩が取れます。';
		$TourDetails_Options_Label = 'オプション';
		$TourDetails_Options_Desc = '雪崩安全セットとバックパックは¥5,000/1日です。用具レンタルのキャンセル料は予約日の2日前までの17時から100％かかります。';
		
		
		
		
		
		// tour daily schedule
		$TourSchedule_Subtitle = 'ツアープラン';
		$TourSchedule_Title = '一日の流れ';
		$TourSchedule_Body = '天候、参加者、雪質で毎日の流れが変わります。以下のようなスケジュールが一般的なものです。ただし、朝９時までにバックカントリーセンターに到着してください。リードガイドの判断で変更、休止、中止になる場合があります。';
		
		$TourSchedule_Step1 = '全員がバックカントリーセンターに集まる。苗場プリンスホテル、または苗場の街での宿泊施設にお迎えが必要な方は予約時にお申し付け下さい。';
		$TourSchedule_Step2 = '道具・知識の確認、安全講習、スケジュール確認等';
		$TourSchedule_Step3 = 'キャット乗り場に出発';
		$TourSchedule_Step4 = '旧スキー場ベースでビーコンサーチ練習';
		$TourSchedule_Step5 = '滑走開始';
		$TourSchedule_Step6 = '登りの最終キャット';
		$TourSchedule_Step7 = '終了・解散';
		$TourSchedule_Step8 = '';
		
		
		
		
		
		
		// safety equipment
		$SafetyEquipment_Title = '道具は何が必要？';
		$SafetyEquipment_Body = '参加者にはアバランチ安全装備を備えたリュックサックが必要です。 レンタルもあります（¥5,000/1日）。レンタル申し込みはご注文の際にオプションとして予約できます。ツアー前に装備の確認をさせていただきます。';
		
		$SafetyEquipment_Beacon_Label = 'ビーコン';
		$SafetyEquipment_Beacon_Desc = '近代のデジタルビーコン。電池の残量が十分で、今シーズン入れ替えったもの。<br>
<a class="link-blue" target="_blank" href="https://www.mammut.ch/INT/en/B2C-Kategorie/Women/ELEMENT-Barryvox/p/2710-00040-1012-1">Mammut Element Barryvox</a> または <a class="link-blue" href="https://www.amazon.com/Backcountry-Access-Tracker-Avalanche-Beacon/dp/B000YUATNO">DTS Avalanche Beacon</a> がおすすめです。';
		$SafetyEquipment_Probe_Label = 'スノープローブ';
		$SafetyEquipment_Probe_Desc = '220-300cmの長さ<br>
<a class="link-blue" target="_blank" href="https://www.ortovox.com/uk/shop/avalanche-emergency-equipment/avalanche-probes/">Ortovox 240</a> がおすすめです。';
		$SafetyEquipment_Shovel_Label = 'スコップ';
		$SafetyEquipment_Shovel_Desc = 'アルミニウムまたは軽金属のリュックに入るもの<br>
<a class="link-blue" target="_blank" href="http://www.pieps.com/en/product/pieps-shovel-tour-i-tour-t">Pieps Tour</a> がおすすめです。';

		$SafetyEquipment_Backpack_Label = 'リュック';
		$SafetyEquipment_Backpack_Desc = 'アバランチ安全装備、カメラ、水筒、食事が入る';
		$SafetyEquipment_OtherGear_Label = 'その他';
		$SafetyEquipment_OtherGear_Desc = 'エアバッグなどの他のアバランチギアがある場合は是非ご持参ください。持ってこられる方はガイドにお申し付けください。';
		
		
		
		
		
		// recommended gear
		$RecommendedGear_Title = 'どんなスキーギアをお勧めしますか？';
		$RecommendedGear_Body = '山では突然天気が変わることが多いです。特に寒さ対策として、小さく収納できる暖かい服を持ってくることをお勧めします。';
		
		$RecommendedGear_Helmet_Label = 'ヘルメット';
		$RecommendedGear_Helmet_Desc = 'ウィンタースポーツ用に限る。';
		$RecommendedGear_Skis_Label = 'スキー';
		$RecommendedGear_Skis_Desc = 'オールマウンテンまたはバックカントリー用のもの、幅が90mm以上のもの。';
		
		$RecommendedGear_SkiPoles_Label = 'ストック';
		$RecommendedGear_SkiPoles_Desc = 'パウダーバスケット付きのものがお勧め。';
		$RecommendedGear_Snowboard_Label = 'スノーボード';
		$RecommendedGear_Snowboard_Desc = 'オールマウンテンまたはパウダーボードがお勧め。伸縮できるストックもお勧め。';
		$RecommendedGear_SkiWear_Label = 'ウエア';
		$RecommendedGear_SkiWear_Desc = '完全防水のアウターとベースレーヤーミッドレーヤー、天気が変わりやすいので着たり脱いだりできるように数枚持って着てください。';
		$RecommendedGear_Facemask_Label = 'フェイスマスク';
		$RecommendedGear_Facemask_Desc = 'お勧めです。降雪時も滑りますので、フェイスマスクが大活躍。';
		
		$RecommendedGear_Goggles_Label = 'アイウェア';
		$RecommendedGear_Goggles_Desc = 'ゴーグルとサングラス両方お勧めします。';
		
		$RecommendedGear_RentingGear_Label = 'ギアのレンタル';
		$RecommendedGear_RentingGear_Desc = '苗場プリンスホテル内または苗場のタウンに複数のレンタルショップがあります。朝早くから行列ができる店も多く、ツアー前日にレンタルをすることをお勧めします。';
		
		
		// how to get here
		$HowToGetHere_Title = '三国キャットへのアクセス';
		$HowToGetHere_Body = '三国キャットの事務所は新潟県湯沢町、苗場地区にある。苗場プリンスホテルから車で２分です。アクセスとしては上越新幹線で越後湯沢駅で下車し、路線バスで苗場プリンスホテル向けのものに乗ってください。苗場プリンスホテルまでお迎えに行きます。車の方は東京方面から月夜野出口を利用し、新潟方面から湯沢出口を利用し、出口付近に苗場への案内看板があります。もしくは、苗場プリンスホテルをナビに入れて下さい。';
		
		
		
		// additional questions
		$AddlQuestions_Title = 'そのほかに分からないことがあれば、遠慮なくお尋ねてください！';
		$AddlQuestions_Body = '分からないことがありましたらお気軽に <a href="mailto:yoyaku@mikuni-cat.com">yoyaku@mikuni-cat.com</a> までご連絡ください。</p>';
		
		
		// testimonials
		
		
		
		// footer
		$Footer_PhoneText = 'JP_Phone: +81 (0)70-5550-8807';
		$Footer_MailText = 'お問い合わせメール： <a class="link-blue" href="mailto:yoyaku@mikuni-cat.com">yoyaku@mikuni-cat.com</a>';
		$Footer_OfficeText = 'JP_Office hours: Weekdays, 8:30 to 16:00<br>Weekends/holidays, 8:00 to 17:00';
		
	}
?>