<?php
	// utility script to set the $_SESSION language, called by all pages
										 
	// default user session is English
	if ((isset($_SESSION['UserLang'])) && ($_SESSION['UserLang'] == 'JP')){
		$_SESSION['UserLang'] = 'JP';
	} else {
		$_SESSION['UserLang'] = 'EN';
	}
	
	// see if user has reset session language to EN, if so set session language to EN
	if ((isset($_GET['UserLang'])) && ($_GET['UserLang'] == 'EN')) {
		$_SESSION['UserLang'] = 'EN';
	} 
	// for JP language reset
	if ((isset($_GET['UserLang'])) && ($_GET['UserLang'] == 'JP')) {
		$_SESSION['UserLang'] = 'JP';
	}
	
	
?>