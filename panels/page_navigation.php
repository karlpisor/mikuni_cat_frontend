<div class="container page-nav">
        <div class="row">
        	<div class="col-md-2 col-md-offset-1 faq-container">
        		<a class="link-question-marks" href="#faq-content">
        		<div class="question-mark-icons">
	        	<i class="fa fa-question fa-3x"></i><i class="fa fa-question fa-5x"></i><i class="fa fa-question fa-3x"></i>
	        	</div>
	        	<h4 class="faq content-nav-title font-helvetica centered"><?php echo $Nav_Questions; ?></h4>
	        	</a>
        	</div>
        	<div class="col-md-2 col-md-offset-1 availability-container">
        		
        		<a class="link-availability" href="<?php echo $BookNow_Email_HREF; ?>">
        		<div class="calendar-icon">
	        	<i class="fa fa-calendar fa-4x"></i>
	        	</div></a>
	        	
	        	<h4 class="availability content-nav-title font-helvetica centered">
	        	<a href="<?php echo $BookNow_Email_HREF; ?>"><?php echo $Nav_Availability; ?></a>
	        	</h4>
	        	
        	</div>
        	<div class="col-md-2 col-md-offset-1 yen-sign-container">
        		<a class="link-faq" href="#pricing-details-content">
        		<div class="yen-sign-icon">
				<i class="fa fa-jpy fa-4x"></i>
				</div>
				<h4 class="pricing-details content-nav-title font-helvetica centered"><?php echo $Nav_Pricing_Details; ?></h4>
				</a>
			</div>
        	
        </div><!--/row-->
      </div><!--/container-->